<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;

/**
 * ContactForm is the model behind the contact form.
 */
class CreateTaskModel extends Model
{

    public $name;
    public $short_description;
    public $full_description;



    public function rules()
    {
        return [
            [['name', 'short_description', 'full_description'], 'required'],
            ];
    }


    public function index()
    {

    }

    public function createTask()
    {
        $task = new WorkWhithDB();

        if(!$this->validate()) {
            return false;
        }
        if(!$task->creatingTask($this->getAttributes())) {
            return false;
        };
        return true;
    }
}
