<?php


namespace app\models;


use yii\db\ActiveRecord;

class WorkWhithDB extends ActiveRecord
{
    public static function tableName()
    {
        return '{{crud}}';
    }

    public function rules()
    {
        return [
            [['name'], 'unique'],
        ];
    }


    public function creatingTask($arr)
    {
        $this->setAttributes($arr);

        $this->create_date = time();

        if($this->save()) {
            return false;
        }
        return true;
    }



}