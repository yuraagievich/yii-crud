<?php

use yii\db\Migration;

/**
 * Class m181105_202318_create_table_tasks
 */
class m181105_202318_create_table_tasks extends Migration
{
    CONST TABLE_TASKS = '{{crud}}';

    public function safeUp()
    {
       $this->createTable(self::TABLE_TASKS,[
          'id' => $this->primaryKey(),
           'name'=> $this->string(32),
           'short_description' => $this->string(120),
           'full_description' => $this->text(),
           'create_date' => $this->integer(11),
           'complete_date' => $this->integer(11),
           'status_task' => $this->integer(1)
       ]);
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_TASKS);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181105_202318_create_table_tasks cannot be reverted.\n";

        return false;
    }
    */
}
