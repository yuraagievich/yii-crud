<?php

/*@var $task  app\models\TaskModel*/


echo $url = Yii::$app->urlManager->createUrl(['task/index']);
$this->title =  $url;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="task-index">
    <h1>Полное описание задания: <?php echo $task->name ?></h1>

<p class="fullDescr">
    <?php echo $task->full_description?$task->full_description: 'Описание отсутствует...'?>
<div>
    <p>Когда создан:
        <?php echo Yii::$app->formatter->format($task->create_date, 'datetime');  ?>
    </p>

    <p>Когда выполнен:
        <?php echo Yii::$app->formatter->format($task->complete_date, 'datetime'?'datetime': ' Активен ') ?>
    </p>
</div>
<p>
    <span>
         <a href="<?php echo Yii::$app->urlManager->createUrl(['task/index'])?>" class="aLinkGeneralPage">Назад&nbsp;</a>
        <a href="<?php echo Yii::$app->urlManager->createUrl(['task/update']).'?id='.$task->id?>" class="aLinkGeneralPage">Обновить&nbsp;</a>
        <a href="<?php echo Yii::$app->urlManager->createUrl(['task/delete']).'?id='.$task->id?>" class="aLinkGeneralPage">Удалить&nbsp;</a>
        <a href="<?php echo Yii::$app->urlManager->createUrl(['task/complete']).'?id='.$task->id?>" class="aLinkGeneralPage">Завершить&nbsp;</a>
    </span>
</p>
</div>