<?php

/* @var $this yii\web\View */
/* @var $task app\models\TaskModel */

use yii\helpers\Url;

echo $url = Yii::$app->urlManager->createUrl(['task/create']);
$this->title =  $url;


$this->params['breadcrumbs'][] = $this->title;
?>

<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>
<div class="task-index">
<a href="<?php echo Url::to(['task/index'])?>" class="aLinkGeneralPage"">На главную</a>
<?php /*echo '<pre>'. print_r($task).'<pre>'; exit()*/?>

<?php $form = ActiveForm::begin(); ?>
<?php echo $form->field($task, 'name'); ?>
<?php echo $form->field($task, 'short_description'); ?>
<?php echo $form->field($task, 'full_description')->textarea(); ?>
<?php echo HTML::submitButton('Сохранить задание', ['class'=> 'btn aLinkGeneralPage']); ?>
<?php ActiveForm::end() ?>



</div>