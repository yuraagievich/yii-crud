<?php


/* @var $this yii\web\View */
/* @var $task app\models\TaskModel */


echo $url = Yii::$app->urlManager->createUrl(['task/index']);
$this->title =  $url;
$this->params['breadcrumbs'][] = $this->title;
?>

<h1>Update Task</h1>

<?php
    use yii\helpers\Url;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>

<div class="task-index">
<a href="<?php echo Url::to(['task/index'])?>" class="aLinkGeneralPage">На главную</a>


<?php $form = ActiveForm::begin(); ?>
<?php echo $form->field($task, 'name'); ?>
<?php echo $form->field($task, 'short_description'); ?>
<?php echo $form->field($task, 'full_description')->textarea(); ?>
<?php echo HTML::submitButton('Update', ['class'=> 'btn btn-primary']); ?>
<?php ActiveForm::end() ?>

</div>

