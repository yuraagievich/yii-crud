<?php

/* @var $this yii\web\View */
/* @var $tasks app\models\TaskModel*/


use yii\helpers\Url;


echo $url = Yii::$app->urlManager->createUrl(['task/index']);
$this->title =  $url;

?>

<div class="task-index">

    <?php $this->params['breadcrumbs'][] = $this->title; ?>

    <h1>Все задания</h1>

    <a href="<?php echo Url::to(['task/create'])?>" class="aLinkGeneralPage">
        Create New Task
    </a>

    <div class="taskContainer">
        <div class="task">
            <div class="taskHeader">
                <div class="taskId">ID</div>
                <div class="taskName">Name</div>
                <div class="taskShortDescr">Краткое описание</div>
                <div class="taskCreateDate">Дата создания</div>
                <div class="taskCompleteDate">Дата выполнения</div>
                <div class="tasknavigateButton">Вспомогательные кнопки</div>
            </div>
            <?php
                foreach ($tasks as $elem) { ?>
                    <div class="taskContent">

                        <div class="taskId">
                            <?php echo $elem->id?>
                        </div>

                        <div class="taskName">
                            <!--<a href="<?php /*echo Url::to(['task/task', 'id' => $elem->id])*/?>"><?php /*echo $elem->name*/?></a>-->
                            <a href="<?php echo Url::to(['task', 'id' => $elem->id])?>"><?php echo $elem->name?></a>
                        </div>

                        <div class="taskShortDescr">
                            <?php echo $elem->short_description?>
                        </div>

                        <div class="taskCreateDate">
                            <?php echo Yii::$app->formatter->format($elem->create_date, 'datetime');?>
                        </div>

                        <div class="taskCompleteDate">
                            <?php echo Yii::$app->formatter->format($elem->complete_date, 'datetime'?'datetime':'nullDisplay') ?>
                        </div>


                        <div class="tasknavigateButton">

                            <form action="<?php echo Url::to(['task/update'])?>" method="GET">
                                <input type="submit" value="Update"  class="aLinkGeneralPage">
                                <input type="hidden" name="id" value="<?php echo $elem->id?>">
                            </form>

                            <!--<form action="<?php /*echo Url::to(['task/delete'])*/?>" method="GET" >-->
                            <form action="delete" method="GET" >
                                <input type="submit" value="Delete" class="aLinkGeneralPage">
                                <input type="hidden" name="id" value="<?php echo $elem->id?>">
                            </form>

                            <form action="<?php echo Url::to(['task/complete'])?>" method="GET" >
                                <input type="submit" value="Complite" class="aLinkGeneralPage">
                                <input type="hidden" name="id" value="<?php echo $elem->id?>">
                            </form>

                        </div>

                    </div>
                <?php }?>
        </div>
    </div>
</div>


