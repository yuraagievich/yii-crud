<?php

namespace app\controllers;


use app\models\WorkWhithDB;
use Faker\Provider\DateTime;
use Psr\Log\InvalidArgumentException;
use yii\web\Controller;
use app\models\CreateTaskModel;
use yii;
use yii\base\Exception;
use yii\i18n\Formatter;


class TaskController extends Controller
{

    public function actionIndex()
    {
        $query = WorkWhithDB::find()->all();
        return $this->render('index', ['tasks' => $query]);
    }


   /*public function actionCreate()
   {
       $task = TaskModel::createTask();
       if($task->load(Yii::$app->request->post()) && $task->validate()) {
           $date = time();
           $task->create_date = $date;
           $task->save();
           Yii::$app->session->setFlash('success', 'Данные сохранены');
           return $this->refresh();
       }
       return $this->render('create', ['task' => $task]);
   }*/

   public function actionCreate()
   {
       $task = new CreateTaskModel();
       $flag = $task->load(Yii::$app->request->post());

       if($flag) {

           $task->createTask();
           Yii::$app->session->setFlash('success', 'Данные сохранены');
           return $this->refresh();
       }
       return $this->render('create', ['task' => $task]);
   }




    public function actionUpdate($id)
    {
        $task = new CreateTaskModel();
        $task->load(Yii::$app->request->post());
        if(!$task) {
            $date = time();
            $task->create_date = $date;
            $task->save();
            Yii::$app->session->setFlash('success', 'Данные обновлены');
            return $this->refresh();
        }
        return $this->render('update', ['task' => $task]);
/*
        $this->blaBlaBla ($id, 'Данные обновлены', 'update');*/


    }




   /* public function blaBlaBla ($id, $textMessage, $renderingView) {

        $task = TaskModel::findOne($id);
        if($task->load(Yii::$app->request->post())) {
            $date = time();
            $task->create_date = $date;
            $task->save();
            Yii::$app->session->setFlash('success', $textMessage);
            return $this->refresh();
        }
        return $this->render($renderingView, ['task' => $task]);
    }*/





    public function actionDelete($id)
    {
        TaskModel::findOne($id)->delete();
        return $this->redirect('index');
    }

    public function actionComplete($id)
    {
        $task = TaskModel::findOne($id);
        $task->complete_date = time();
        $task->save();
        return $this->redirect('index');
    }

   public function actionTask($id)
   {
        $task = TaskModel::findOne($id);
        return $this->render('task', ['task'=> $task]);
   }





   public static function readVar($var)
   {
       echo '<pre>';
       print_r($var);
       echo '</pre>';
       die;
   }
}
